# �������� ������ 7 � 1 DC 0-100V 10A

�������� ������������������� �������� ����� �������� ����������, ���, �������, �������, � ����� �������� ����������� ����. ����� ����� ������ ����� ���������� ������ � ���������. � ������ ������� ����� ��������� �� 10 ������� ������.

��� ������ ��������� ������� ����� �������������� ������� �� 4 �� 20 �, ��� ����� ���� ��������� ������, ����������� 6 ��� 12 �, �������� ���������� �� ���������� ��������. ��� �� ���� ��� �������� ����� ��������������� �������� ����������� �������.

����� ������ � ���, ��� �� ������� ����������� �������� �����, ��� � ���� ��������� ������������ ��� ��������� � ���������� ������������ ��������.

![](401948101_401948101.jpg)



## ��������������

| ��������                                     |           |
| -------------------------------------------- | --------- |
| ������ �������������                         | �����     |
| ���                                          | ��������  |
| ���������                                    | �����     |
| ��������� ����������                         |           |
| ������� ��������� ����������� ����������     | �         |
| ������������ �������� ����������� ���������� | 100       |
| ������� ��������� ����������� ����������     | �         |
| ��������� ����                               |           |
| ������� ��������� ����������� ����           | �         |
| ������������ �������� ����������� ����       | 10        |
| ������� ��������� ����������� ����           | �         |
| �������                                      |           |
| �������                                      | OLED      |
| ������ ��������                              | 0.96 ���� |



| �����                                        |                    |
| -------------------------------------------- | ------------------ |
| ����������� ������� �����������              | -10 ��             |
| ������������ ������� �����������             | 65 ��              |
| �����                                        | 48 ��              |
| ������                                       | 29 ��              |
| ������                                       | 26 ��              |
| ���                                          | 0.02 ��            |
| ���������������� ��������������              |                    |
| �������� ��������� ����������                | 00.0-99.9 V        |
| ����                                         | ����������         |
| �������                                      | 0.96" OLED         |
| �������                                      | �� 4 �� 20 �����   |
| �������� ��������� ����������                | � (0.5% + 1 �����) |
| �������� ��������� ����                      | 0.00-9.99 �        |
| ���������� ��������                          | 5                  |
| ���������������                              | STM8S003F3P6       |
| ������ ��������� � ������� ��� ���������     | 45�� * 26��        |
| �������� ��������� ����                      | � (1% + 2 �����)   |
| �������� ��������� �������                   | 00.000-99.999Ah    |
| �������� ��������� �������                   | 0-9999.9Wh         |
| �������� ��������� �����������               | -15�c - 60�c       |
| �������� ��������� ��������                  | 00.00-99.99-999.9W |
| �������� ��������� �������                   | 0-99�.59���        |
| ����� ��������:                              | 18.5 ��            |





## ����������

�������� ������������������� ������������������ �������������� � �������������� ���������� �� �������. �� ��-�� �������� ������������� ����������� ��� ������� �������� ���������� ����� ���������� ����������� � ����������, �������� ������ ����� �� ���������� ���� ��� ��������. ��� �� ����������� � ������ ����� ����������,����� �������� �������� ��� 8-9 �, � ����� ����� ����� �������� ��� �� 1��.

��� ����������� ���� ������ �����:

1. ��������� ������ �� �������.
2. ��������� ��������.
3. ������ � ������� ������.
4. ���������� �������� ��������� ��������� ��������� ������� �� ������� - ���.�
5. ��������� ������.�
6. ���������� ���������.



![](686411956_686411956.jpg)





## ����� �����������

![](686382158_686382158.jpg)



![](686382649_686382649.jpg)






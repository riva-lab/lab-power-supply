#ifndef DRIVERS_H
    #define	DRIVERS_H

    #include "stdint.h"
    #include "io_defines.h"
    #include "uart/io_defines.h"

// ================== КОНФИГУРИРОВАНИЕ АППАРАТНЫХ СРЕДСТВ ======================

// Конфигурация выводов МК
    #define PIN_NTC1                DEFINE_PORT(C, 1)   // термистор NTC 1
    #define PIN_NTC2                DEFINE_PORT(C, 2)   // термистор NTC 2
    #define PIN_PWM                 DEFINE_PORT(C, 5)   // ШИМ-выход управления нагрузкой
    #define PIN_PWM_TRIS            DEFINE_TRIS(C, 5)   // ШИМ-выход управления нагрузкой

    #define PIN_DEBUG               DEFINE_PORT(C, 0)
    #define PIN_DEBUG_TRIS          DEFINE_TRIS(C, 0)

// подключаемые линии к АЦП
    #define ADC_NTC1                (5 << 2)            // канала для NTC 1 (AN5)
    #define ADC_NTC2                (6 << 2)            // канала для NTC 2 (AN6)

// аппаратные настройки цепей замера
    #define R_NTC                   30.0    // [кОм]    резистор подтяжки терморезистора
    #define NTC_RES(x)              (uint8_t)(R_NTC * x / (1024 - x))

    #define PWM_MIN                 0
    #define PWM_MAX                 80
    #define T_MIN                   35
    #define T_MAX                   85
    #define PWM_MAP(t)              (uint8_t)((t - T_MIN) * (PWM_MAX - PWM_MIN) / (T_MAX - T_MIN)) + PWM_MIN


    #define NTC_TEMP_R_START        10  // [кОм]    сопротивление, которое соответствует началу массива температур
    #define NTC_TEMP_R_END          59  // [кОм]    сопротивление, которое соответствует концу массива температур

// массив температур, индекс - сопротивление (0=10к, 1=11к, ..., до 59к)
const uint8_t ntc_temp[] = {
                            83, 81, 78, 76, 74, 72, 70, 68, 67, 65,
                            64, 63, 61, 60, 59, 58, 57, 56, 55, 54,
                            53, 53, 52, 51, 50, 49, 49, 48, 47, 47,
                            46, 46, 45, 44, 44, 43, 43, 42, 42, 41,
                            41, 40, 40, 39, 39, 39, 38, 38, 37, 37
};


void inline HW_Ports_Init();                        // инициализация портов ввода/вывода

void inline HW_ADC_Init();                          // инициализация АЦП
uint16_t    HW_ADC_GetValue(uint8_t channel);       // замер напряжения в выбранном канале АЦП

void        HW_PWM_Init();                          // инициализация ШИМ
void        HW_PWM_Set(uint8_t value);              // установка значения ШИМ

uint16_t    HW_NTC_Get_Resistance(uint8_t channel); // получение сопротивления NTC-термистора


#endif	/* DRIVERS_H */


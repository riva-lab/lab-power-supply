#include <stdlib.h>
#include "drivers.h"
#include "main.h"


#define ADC_OFFSET                  1

#define ADCON1_JUSTIFIED_RIGHT      (1 << 7)
#define ADCON1_JUSTIFIED_LEFT       (0 << 7)
#define ADCON1_CLOCK_FOSC_div_2     (0 << 4)
#define ADCON1_CLOCK_FOSC_div_8     (1 << 4)
#define ADCON1_CLOCK_FOSC_div_32    (2 << 4)
#define ADCON1_CLOCK_FRC_V1         (3 << 4)
#define ADCON1_CLOCK_FOSC_div_4     (4 << 4)
#define ADCON1_CLOCK_FOSC_div_16    (5 << 4)
#define ADCON1_CLOCK_FOSC_div_64    (6 << 4)
#define ADCON1_CLOCK_FRC_V2         (7 << 4)
#define ADCON1_VREF_POS_AVDD        (0 << 0)
#define ADCON1_VREF_POS_VREF_EXT    (2 << 0)
#define ADCON1_VREF_POS_FVR         (3 << 0)

#define FVRCON_ENABLED              (1 << 7)
#define FVRCON_DISABLED             (0 << 7)
#define FVRCON_TEMP_ENABLED         (1 << 5)
#define FVRCON_TEMP_DISABLED        (0 << 5)
#define FVRCON_TEMP_RANGE_LOW       (0 << 4)
#define FVRCON_TEMP_RANGE_HIGH      (1 << 4)
#define FVRCON_COMP_DAC_VREF_OFF    (0 << 2)
#define FVRCON_COMP_DAC_VREF_1_024V (1 << 2)
#define FVRCON_COMP_DAC_VREF_2_048V (2 << 2)
#define FVRCON_COMP_DAC_VREF_4_096V (3 << 2)
#define FVRCON_ADC_VREF_OFF         (0 << 0)
#define FVRCON_ADC_VREF_1_024V      (1 << 0)
#define FVRCON_ADC_VREF_2_048V      (2 << 0)
#define FVRCON_ADC_VREF_4_096V      (3 << 0)


void inline HW_Ports_Init();                        // инициализация портов ввода/вывода
void inline HW_ADC_Init();                          // инициализация АЦП
uint16_t    HW_ADC_GetValue(uint8_t channel);       // замер напряжения в выбранном канале АЦП


// инициализация портов ввода/вывода
void inline HW_Ports_Init()
{
    ANSELA  = 0b00000000;
    ANSELC  = 0b00000110;

    PORTA   = 0b00000000;
    PORTC   = 0b00000000;

    TRISA   = 0b11000000;
    TRISC   = 0b11000110;

    WPUA    = 0b00000000;
    WPUC    = 0b00000000;       // подтяжка

    OPTION_REGbits.nWPUEN = 1;  // подтяжка выкл
}

// инициализация АЦП
void inline HW_ADC_Init()
{
    //    // ADC FVR output is 2x (2.048V)
    //    FVRCON  = FVRCON_DISABLED + FVRCON_ADC_VREF_2_048V;

    // VREF+ is connected to internal FVR module
    ADCON1  = ADCON1_CLOCK_FRC_V1 + ADCON1_JUSTIFIED_RIGHT + ADCON1_VREF_POS_AVDD;
}

// замер напряжения в выбранном канале АЦП
uint16_t HW_ADC_GetValue(uint8_t channel)
{
    ADCON0              = channel;
    ADCON0bits.ADON     = 1;        // АЦП включен
    __delay_us(25);                 // пауза для заряда конд-ра АЦП

    ADCON0bits.GO_nDONE = 1;        // запуск АЦП
    while (ADCON0bits.GO_nDONE);    // ожидание окончания преобразования

    ADCON0bits.ADON     = 0;

    ADRESL < ADC_OFFSET ?
            ADRESL  = 0 :
            ADRESL -= ADC_OFFSET;

    return (ADRESH << 8) + ADRESL;
}

// инициализация ШИМ
void HW_PWM_Init()
{
    PIN_PWM_TRIS        = 1;
    PR2                 = 62; // период
    CCP1CON             = 0b00001100;
    CCPR1L              = 0; // коэф.заполнения

    PIR1bits.TMR2IF     = 0;
    T2CON               = 0b00000000;
    T2CONbits.TMR2ON    = 1;

    while (!PIR1bits.TMR2IF);
    PIN_PWM_TRIS = 0;
}

// установка значения ШИМ
void HW_PWM_Set(uint8_t value)
{
    CCPR1L              = value >> 2;
    CCP1CONbits.DC1B    = value & 0b11;

    //    uint8_t low = (value & 0b11);
    //    CCP1CON    &= ~0b00110000;
    //    CCP1CON    |= (low << 4);

    //    CCP1CONbits.DC1B0 = value & 1;
    //    value >>= 1;
    //    CCP1CONbits.DC1B1 = value & 1;
    //    value >>= 1;
    //    CCPR1L              = value;
}

//uint8_t ntc100k_r[1] = {
//};

// получение сопротивления NTC-термистора
uint16_t HW_NTC_Get_Resistance(uint8_t channel)
{
    uint8_t ch;
    if (channel == 1) ch = ADC_NTC1;
    if (channel == 2) ch = ADC_NTC2;
    
    return NTC_RES(HW_ADC_GetValue(ch));
}
#ifndef MAIN_H
    #define MAIN_H

    #include <xc.h>
    #include <stdint.h>
    #include "config.h"
    #include "version.h"

    #define FREQUENCY               8000    // [кГц] системная частота

    #if (FREQUENCY == 8000)
        #define _XTAL_FREQ          8000000 // тактовая частота МК
        #define OSCILLATOR_SETUP    OSCCON  = (0b1110 << 3);    // <6:3> 1110 = 8 MHz
    #elif (FREQUENCY == 500)
        #define _XTAL_FREQ          500000  // тактовая частота МК
        #define OSCILLATOR_SETUP    OSCCON  = (0b0111 << 3);    // 500k MFINTOSC
    #elif (FREQUENCY == 250)
        #define _XTAL_FREQ          250000  // тактовая частота МК
        #define OSCILLATOR_SETUP    OSCCON  = (0b0110 << 3);    // 250k MFINTOSC
    #endif

    #define TMR0_TIME               10               // [мс] установка таймера
    #define TMR0_VALUE              (uint8_t)(0xFF - (TMR0_TIME * (_XTAL_FREQ / 4000.0 / 256.0)) + 1)
    #define COUNTER_1S              (uint8_t)(1000.0 / TMR0_TIME) // счетчик 1 с

#endif
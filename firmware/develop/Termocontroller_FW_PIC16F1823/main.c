#include <pic.h>

#include "main.h"
#include "drivers.h"
#include "uart/uart_tx.h"
#include "uart/uart.h"


bit tmr_1s;

// инициализация системы
void System_Init()
{
    INTCONbits.GIE      = 0;
    WDTCONbits.SWDTEN   = 0;

    OSCILLATOR_SETUP;           // системный генератор 8 MHz
    HW_Ports_Init();            // инициализация портов ввода/вывода
    HW_ADC_Init();              // инициализация АЦП
    HW_PWM_Init();              // инициализация ШИМ

    WDTCONbits.SWDTEN   = 1;
    INTCONbits.GIE      = 1;
}

// инициализация системного таймера
void System_Timer_Init()
{
    OPTION_REG         &= ~0b00111111;
    OPTION_REG         |=  0b00000111;  // настройка TMR0 1/256
    TMR0                =  TMR0_VALUE;

    INTCONbits.TMR0IF   = 0;
    INTCONbits.TMR0IE   = 1;            // включение прерываний TMR0
}

// обработчик прерывания системного таймера
void System_Timer_ISR()
{
    if (INTCONbits.TMR0IE && INTCONbits.TMR0IF)
    {
        TMR0                = TMR0_VALUE;
        INTCONbits.TMR0IF   = 0;

        static uint8_t cnt_1s = 0;
        if (cnt_1s) cnt_1s--;
        else
        {
            cnt_1s = COUNTER_1S;
            tmr_1s = 1;
        }
    }
}

// прерывания
void interrupt System_ISR()
{
    System_Timer_ISR();
}

//
void Load_Control()
{
    uint8_t r1 = HW_NTC_Get_Resistance(1);
    uint8_t r2 = HW_NTC_Get_Resistance(2);

    uint8_t r = r1;
    if (r2 < r1) r = r2;

    UART_Send_String("R1 = ");
    UART_Send_UInt8(r1);
    UART_Send_String("k; ");
    UART_Send_String("R2 = ");
    UART_Send_UInt8(r2);
    UART_Send_String("k; ");

    if (r < NTC_TEMP_R_START)       // перегрев
    {
        HW_PWM_Set(PWM_MAX);
        UART_Send_String("FIRE");
    } else if (r > NTC_TEMP_R_END)  // холодный, t<37C
    {
        HW_PWM_Set(PWM_MIN);
        UART_Send_String("COLD, <37C");
    } else                          // пропорциональный ШИМ
    {
        uint8_t t   = ntc_temp[r - NTC_TEMP_R_START];
        uint8_t pwm = PWM_MAP(t);
        HW_PWM_Set(pwm);

        UART_Send_String("t = ");
        UART_Send_UInt8(t);
        UART_Send_String(" C; PWM: ");
        UART_Send_UInt8(pwm);
    }

    UART_Send_String("\r\n");
}

// старт программы
void main()
{
    System_Init();
    System_Timer_Init();

    UART_Open_Tx(0);
    UART_Set_Baudrate(57600);
    UART_Send_String("TC Start.\r\n");

    //    PIN_DEBUG_TRIS = 0;
    //    PIN_DEBUG ^= 1;

    while (1)
    {
        CLRWDT();

        if (tmr_1s)
        {
            tmr_1s = 0;

            Load_Control();
        }
    }
}

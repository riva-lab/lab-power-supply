#ifndef IO_DEFINES_H
    #define	IO_DEFINES_H

    #define CONCAT2__(A1, A2)               A1 ## A2
    #define CONCAT2(A1, A2)                 CONCAT2__(A1, A2)

    #define CONCAT3__(A1, A2, A3)           A1 ## A2 ## A3
    #define CONCAT3(A1, A2, A3)             CONCAT3__(A1, A2, A3)

    #define CONCAT5__(A1, A2, A3, A4, A5)   A1 ## A2 ## A3 ## A4 ## A5
    #define CONCAT5(A1, A2, A3, A4, A5)     CONCAT5__(A1, A2, A3, A4, A5)


    #define DEFINE_TRIS(port, pin)  CONCAT5(TRIS, port, bits.TRIS, port, pin)
    #define DEFINE_LATCH(port, pin) CONCAT5(LAT,  port, bits.LAT,  port, pin)
    #define DEFINE_PORT(port, pin)  CONCAT5(PORT, port, bits.R,    port, pin)
    #define DEFINE_ANSEL(port, pin) CONCAT5(ANSEL, port, bits.ANS, port, pin)

#endif	/* IO_DEFINES_H */


#include <xc.h>
#include <stdint.h>
#include "uart.h"

uint8_t dummy_byte;

// установка скорости передатчика/приемника
void UART_Set_Baudrate(uint16_t br)
{
    BAUDCONbits.BRG16   = 1;
    TXSTAbits.BRGH      = 1;

    uint16_t value      = (_XTAL_FREQ / 4) / br - 1;
    SPBRGH              = (value >> 8);
    SPBRGL              = value;
}

//
void UART_AutoBaudDetect_Start()
{
    //    uint8_t rx_int_tmp  = PIE1bits.RCIE;
    BAUDCONbits.ABDEN   = 1;

    while (BAUDCONbits.ABDEN) CLRWDT();

    dummy_byte          = RCREG;
    //    PIE1bits.RCIE       = rx_int_tmp;
}


// деинициализация передатчика и приемника
void UART_Close_TxRx()
{
    //    TXSTAbits.TXEN = 0;
    //    RCSTAbits.SPEN = 0;
    TXSTA       = 0;
    RCSTA       = 0;
    BAUDCON     = 0;
}

//
void uart_test()
{
    UART_Send_UInt8(123);
    UART_Send_String(" 8bit\n\x0D");
    UART_Send_UInt8(23);
    UART_Send_String(" 8bit\n\x0D");
    UART_Send_UInt8(0);
    UART_Send_String(" 8bit\n\x0D");
    UART_Send_UInt8(10);
    UART_Send_String(" 8bit\n\x0D");
    UART_Send_UInt8(100);
    UART_Send_String(" 8bit\n\x0D");

    UART_Send_UInt16(12345);
    UART_Send_String(" 16bit\n\x0D");
    UART_Send_UInt16(123);
    UART_Send_String(" 16bit\n\x0D");
    UART_Send_UInt16(0);
    UART_Send_String(" 16bit\n\x0D");
    UART_Send_UInt16(10);
    UART_Send_String(" 16bit\n\x0D");
    UART_Send_UInt16(1000);
    UART_Send_String(" 16bit\n\x0D");
}
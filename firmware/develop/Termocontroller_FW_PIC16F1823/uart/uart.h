#ifndef UART_H
    #define	UART_H

    #include "uart_rx.h"
    #include "uart_tx.h"

    #define BIT9_IS_ADDRESS         1
    #define BIT9_ISNT_ADDRESS       0


// установка скорости передатчика/приемника
void UART_Set_Baudrate(uint16_t br);

//
void UART_AutoBaudDetect_Start();

// деинициализация передатчика и приемника
void UART_Close_TxRx();


#endif	/* UART_H */


#include <xc.h>
#include <stdint.h>
#include "uart.h"

// инициализация передатчика
void UART_Open_Tx(uint8_t invert)
{
    UART_TX_PORT_SET;

    TXSTAbits.SYNC      = 0;
    RCSTAbits.SPEN      = 1;
    BAUDCONbits.SCKP    = invert;   // инверсия

    TXSTAbits.TXEN      = 1;
}

//
void UART_Close_Tx()
{
    TXSTAbits.TXEN      = 0;
}


// ожидание окончания передачи
void UART_Wait_If_Transmitting()
{
    while (!TXSTAbits.TRMT) CLRWDT();
}

//
void UART_Send_Break()
{
    TXSTAbits.SENDB = 1;
    TXSTAbits.TX9   = 0;

    UART_Wait_If_Transmitting();

    TXREG           = 0;
}

// передача 8-битных данных
void UART_Tx_8bit(uint8_t data)
{
    TXSTAbits.TX9   = 0;

    UART_Wait_If_Transmitting();

    TXREG           = data;
}

// передача 9-битных данных
void UART_Tx_9bit(uint8_t data, uint8_t bit9)
{
    TXSTAbits.TX9   = 1;
    TXSTAbits.TX9D  = (bit9 != 0);

    UART_Wait_If_Transmitting();

    TXREG           = data;
}

// передача состояния Break
void UART_Tx_Break()
{
    TXSTAbits.SENDB = 1;

    UART_Wait_If_Transmitting();

    TXREG           = 0;
}

// передача адресного байта
void inline UART_Send_Address(uint8_t address)
{
    UART_Tx_9bit(address, BIT9_IS_ADDRESS);
}

// передача 1 байта
void inline UART_Send_Data_AddrMode(uint8_t data)
{
    UART_Tx_9bit(data, BIT9_ISNT_ADDRESS);
}

// передача NULL-terminated строки в адресном режиме
void UART_Send_String_AddrMode(const char * string)
{
    while ((*string) != 0)
        UART_Send_Data_AddrMode(*(string++));
}

// передача NULL-terminated строки
void UART_Send_String(const char * string)
{
    while ((*string) != 0)
        UART_Tx_8bit(*(string++));
}

// передача блока данных заданной длины в адресном режиме
void UART_Send_Buffer_AddrMode(const uint8_t * buffer, uint8_t length)
{
    while (length-- != 0)
        UART_Send_Data_AddrMode(*buffer);
}

// передача блока данных заданной длины
void UART_Send_Buffer(const uint8_t * buffer, uint8_t length)
{
    while (length-- != 0)
        UART_Tx_8bit(*buffer);
}

// передача 8-разрядного целого значения в 10-чном виде в адресном режиме
void UART_Send_UInt8_AddrMode(uint8_t data)
{
    #if 0
    uint8_t x[3], i = 0;

    do      x[i++] = data % 10, data /= 10; while (data);

    while   (i--) UART_Send_Data_AddrMode(x[i] + 0x30);

    #else
    uint8_t mod, not_empty = 0, base = 100;
    do
    {
        mod     = data / base;
        data   %= base;
        base   /= 10;

        if (mod || not_empty)
            not_empty = 1, UART_Send_Data_AddrMode(mod + 0x30);
    } while (base);

    if (!not_empty) UART_Send_Data_AddrMode(0x30);
    #endif
}

// передача 8-разрядного целого значения в 10-чном виде
void UART_Send_UInt8(uint8_t data)
{
    #if 0
    uint8_t x[3], i = 0;

    do      x[i++] = data % 10, data /= 10; while (data);

    while   (i--) UART_Tx_8bit(x[i] + 0x30);

    #else
    uint8_t mod, not_empty = 0, base = 100;
    do
    {
        mod     = data / base;
        data   %= base;
        base   /= 10;

        if (mod || not_empty)
            not_empty = 1, UART_Tx_8bit(mod + 0x30);
    } while (base);

    if (!not_empty) UART_Tx_8bit(0x30);
    #endif
}

// передача 16-разрядного целого значения в 10-чном виде в адресном режиме
void UART_Send_UInt16_AddrMode(uint16_t data)
{
    #if 01
    uint8_t x[5], i = 0;

    do      x[i++] = data % 10, data /= 10; while (data);

    while   (i--) UART_Send_Data_AddrMode(x[i] + 0x30);

    #else
    uint8_t mod, not_empty = 0, base = 100;
    do
    {
        mod     = data / base;
        data   %= base;
        base   /= 10;

        if (mod || not_empty)
            not_empty = 1, UART_Send_Data_AddrMode(mod + 0x30);
    } while (base);

    if (!not_empty) UART_Send_Data_AddrMode(0x30);
    #endif
}

// передача 16-разрядного целого значения в 10-чном виде
void UART_Send_UInt16(uint16_t data)
{
    #if 01
    uint8_t x[5], i = 0;

    do      x[i++] = data % 10, data /= 10; while (data);

    while   (i--) UART_Tx_8bit(x[i] + 0x30);

    #else
    uint8_t mod, not_empty = 0, base = 100;
    do
    {
        mod     = data / base;
        data   %= base;
        base   /= 10;

        if (mod || not_empty)
            not_empty = 1, UART_Tx_8bit(mod + 0x30);
    } while (base);

    if (!not_empty) UART_Tx_8bit(0x30);
    #endif
}

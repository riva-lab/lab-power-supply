#include <xc.h>
#include <stdint.h>
#include "uart_rx.h"

//
void UART_Open_Rx()
{
    UART_RX_PORT_SET;

    TXSTAbits.SYNC      = 0;
    RCSTAbits.SPEN      = 1;

    RCSTAbits.RX9       = 0;
    RCSTAbits.ADDEN     = 0;
    RCSTAbits.CREN      = 1;
}

//
void UART_Open_Rx_AddrMode()
{
    UART_RX_PORT_SET;

    TXSTAbits.SYNC      = 0;
    RCSTAbits.SPEN      = 1;

    RCSTAbits.RX9       = 1;
    RCSTAbits.ADDEN     = 1;
    RCSTAbits.CREN      = 1;
}

//
void inline UART_Close_Rx()
{
    UART_Rx_Int_Disable();
    RCSTAbits.CREN      = 0;
}

//
void inline UART_Rx_Int_Enable()
{
    INTCONbits.PEIE     = 1;
    PIE1bits.RCIE       = 1;
}

//
void inline UART_Rx_Int_Disable()
{
    PIE1bits.RCIE       = 0;
}

//
void inline UART_AutoWakeUp_Enable()
{
    BAUDCONbits.WUE     = 1;
}

//
void inline UART_AutoWakeUp_Disable()
{
    BAUDCONbits.WUE     = 0;
}

//
void UART_Wait_If_Receiving()
{
    uint8_t timeout = 200; // ~50 ms
    while (!PIR1bits.RCIF && timeout--)
    {
        __delay_us(250);
        CLRWDT();
    }
}

//
uint8_t UART_Receive_8bit()
{
    UART_Wait_If_Receiving();

    if (RCSTAbits.OERR) RCSTAbits.CREN = 0, RCSTAbits.CREN = 1;

    return RCREG;
}

//
uint8_t UART_Receive_9bit()
{
    UART_Wait_If_Receiving();

    if (RCSTAbits.OERR) RCSTAbits.CREN = 0, RCSTAbits.CREN = 1;

    return RCREG;
}

//
uint8_t UART_Receive_Address(uint8_t address)
{
    uint8_t is_address = (UART_Receive_9bit() == address);

    if (is_address)
    {
        RCSTAbits.ADDEN = 0;

        return 1;
    }

    return 0;
}
#ifndef UART_TX_H
    #define	UART_TX_H

    #include <xc.h>
    #include "io_defines.h"
    #include "../main.h"

    #define UART_TX_USE_RA0_NOT_RC4 1

//==============================================================================

    #if (UART_TX_USE_RA0_NOT_RC4 == 1)
        #define UART_TX_PORT_SET        APFCONbits.TXCKSEL = 1; DEFINE_ANSEL(A,0) = 0;
    #else
        #define UART_TX_PORT_SET        APFCONbits.TXCKSEL = 0; DEFINE_ANSEL(C,4) = 0;
    #endif


// инициализация передатчика
void UART_Open_Tx(uint8_t invert);

//
void UART_Close_Tx();

// ожидание окончания передачи
void UART_Wait_If_Transmitting();

//
void UART_Send_Break();

// передача 8-битных данных
void UART_Tx_8bit(uint8_t data);

// передача 9-битных данных
void UART_Tx_9bit(uint8_t data, uint8_t bit9);

// передача состояния Break
void UART_Tx_Break();

// передача адресного байта
void inline UART_Send_Address(uint8_t address);

// передача 1 байта
void inline UART_Send_Data_AddrMode(uint8_t data);

// передача NULL-terminated строки в адресном режиме
void UART_Send_String_AddrMode(const char * string);

// передача NULL-terminated строки
void UART_Send_String(const char * string);

// передача блока данных заданной длины в адресном режиме
void UART_Send_Buffer_AddrMode(const uint8_t * buffer, uint8_t length);

// передача блока данных заданной длины
void UART_Send_Buffer(const uint8_t * buffer, uint8_t length);

// передача 8-разрядного целого значения в 10-чном виде в адресном режиме
void UART_Send_UInt8_AddrMode(uint8_t data);

// передача 8-разрядного целого значения в 10-чном виде
void UART_Send_UInt8(uint8_t data);

// передача 16-разрядного целого значения в 10-чном виде в адресном режиме
void UART_Send_UInt16_AddrMode(uint16_t data);

// передача 16-разрядного целого значения в 10-чном виде
void UART_Send_UInt16(uint16_t data);

#endif	/* UART_TX_H */


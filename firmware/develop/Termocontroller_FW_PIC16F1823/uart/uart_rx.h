#ifndef UART_RX_H
    #define	UART_RX_H

    #include <xc.h>
    #include "io_defines.h"
    #include "../main.h"

    #define UART_RX_USE_RA1_NOT_RC5 1

//==============================================================================

    #if (UART_RX_USE_RA1_NOT_RC5 == 1)
        #define UART_RX_PORT_SET        APFCONbits.RXDTSEL = 1; DEFINE_ANSEL(A,1) = 0;
    #else
        #define UART_RX_PORT_SET        APFCONbits.RXDTSEL = 0; DEFINE_ANSEL(C,5) = 0;
    #endif

//
void UART_Open_Rx();

//
void UART_Open_Rx_AddrMode();

// 
void inline UART_Close_Rx();

//
void inline UART_Rx_Int_Enable();

//
void inline UART_Rx_Int_Disable();

//
void inline UART_AutoWakeUp_Enable();

//
void inline UART_AutoWakeUp_Disable();

//
uint8_t UART_Receive_8bit();

//
uint8_t UART_Receive_9bit();

//
uint8_t UART_Receive_Address(uint8_t address);

#endif	/* UART_RX_H */

